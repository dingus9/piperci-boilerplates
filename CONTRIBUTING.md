# Contributor Guide

Thank you for your interest in PiperCI.

## Getting Started

Clone the [piperci-installer](https://gitlab.com/dreamer-labs/piperci/piperci-installer) repository and run the
`bootstrap` script. Running the `bootstrap` script will make changes to your system. Please do audit the script
to ensure that you know what changes are being made prior to doing so. Generally speaking, this script was written
with the intent to be used on a fresh build or a virtual machine.

Currently, the recommended deployment option (enabled by default) is on Ubuntu 18.04 and OpenFaaS deployed Docker
Swarm.

```bash
git clone git@gitlab.com:dreamer-labs/piperci/piperci-installer.git
sudo ./piperci-installer/bootstrap.sh
```

Installation instructions of individual FaaS's can be found on the respective README.md's. An example of one may be
found [here](https://gitlab.com/dreamer-labs/piperci/piperci-noop-faas)

In order to start a new FaaS, download the latest archived version of the noop FaaS and replace every instance of "noop."

## Git Workflow

Use [trunk-based development](https://trunkbaseddevelopment.com/) when proposing contributions to this repository.
Submit your proposed change as a merge request from a feature branch in your fork.

### Commit Message Guidelines

Always write a [clear log message](https://chris.beams.io/posts/git-commit/) for your commits.

We have very precise rules over how our git commit messages can be formatted.  This leads to **more
readable messages** that are easy to follow when looking through the **project history**.  But also,
we use the git commit messages to generate the `CHANGELOG` based on **angularjs-style commits**.

### Commit Message Format
Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special
format that includes a **type**, a **scope** and a **subject**:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

The **header** is mandatory and the **scope** of the header is optional.

Any line of the commit message cannot be longer 100 characters! This allows the message to be easier
to read on GitHub as well as in various git tools.

### Revert
If the commit reverts a previous commit, it should begin with `revert: `, followed by the header
of the reverted commit.
In the body it should say: `This reverts commit <hash>.`, where the hash is the SHA of the commit
being reverted.

### Type
Must be one of the following:

* **build**: changes that affect the build system or external dependencies
* **ci**: changes to our CI configuration files and scripts
* **chore**: Changes to the build process or auxiliary tools and libraries such as documentation
  generation
* **docs**: Documentation only changes
* **feat**: A new feature
* **fix**: A bug fix
* **perf**: A code change that improves performance
* **refactor**: A code change that neither fixes a bug nor adds a feature
* **revert**: Reverting a previously committed change
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing
  semi-colons, etc)
* **test**: Adding missing or correcting existing tests

### Scope
The scope could be anything specifying place of the commit change. For example `$location`,
`$browser`, `$compile`, `$rootScope`, `ngHref`, `ngClick`, `ngView`, etc...

You can use `*` when the change affects more than a single scope.

### Subject
The subject contains succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize first letter
* no dot (.) at the end

### Body
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes".
The body should include the motivation for the change and contrast this with previous behavior.

### Footer
The footer should contain any information about **Breaking Changes** and is also the place to
[reference GitHub issues that this commit closes][closing-issues].

**Breaking Changes** should start with the word `BREAKING CHANGE:` with a space or two newlines.
The rest of the commit message is then used for this.

A detailed explanation can be found in this [document][commit-message-format].

([Source](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md) commit 55075b8)

Here is what the workflow might look like:
1. [Submit](https://gitlab.com/groups/dreamer-labs/piperci/-/issues) an issue to the relevant project
1. Create fork of target repository, i.e., `piperci-picli`
1. Create a feature branch for your issue on on your fork.
1. Iterate on your change, keeping it as small as possible.  
1. Open a merge request

Once your merge request has been reviewed and approved it will be merged into master.

## Tests

We use `pytest` as our test automation framework. Generally, add adequate unit tests under ``/tests/unit`` per merge
request.

Ensure your tests pass before submitting a merge request in the repository.

To run all tests you should use tox: ``tox``

Going forward, functional (i.e., end-to-end) testing will also be done using ``tox``

## Coding Style

Much of PiperCI is written in Python. For all Python code, we will adhere to PEP8 with a few exceptions. For such
exceptions, see the `python-project\tox.ini.example` configuration file.

We use `flake8` for basic linting checks.

## Coding Standards
 - Code is to run in Python 3.7.3
 - New code should use type hinting for external interfaces intended to be used by other developers. See:
 [PEP 484](https://www.python.org/dev/peps/pep-0484/)
 - Merge requests must pass the standard linting.
 - DRY principles unless it doesn't make sense.
 - Pytest coverage must be greater then `80%`
 - All code must be contained in a valid module with an `__init__.py`
 - Modules should make use of exceptions, functions should not introduce side effects... like `print()` or `sys.exit()`
 - Logging and debug should use the python standard `logging` library

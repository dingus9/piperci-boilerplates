# Standards and best practices

## Standard PiperCI functions
Use the `noop-faas` or `echo-faas` function as a basis for all FaaS functions.

[noop-faas](https://gitlab.com/dreamer-labs/piperci/piperci-noop-faas)
[echo-faas](https://gitlab.com/dreamer-labs/piperci/piperci-echo-faas)

See the docs for developing PiperCI functions.

## Standard toolsets

- bash
- bindep
- coverage
- docker
- docker-swarm
- flake8
  ```
    flake8>=3.6.0,<4
    flake8-bandit
    flake8-debugger
    ; flake8-isort
    flake8-eradicate
    flake8-mutable
  ```
- gitlab-ci
- isort
- pytest
- python3.7.3
- tox
- uwsgi

## Standard licence
  [MIT License](https://gitlab.com/dreamer-labs/piperci/piperci-boilerplates/raw/master/LICENSE)
